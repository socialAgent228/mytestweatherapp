//
//  TableViewCell+.swift
//  TestWeatherApp
//
//  Created by  apple on 4/7/21.
//

import Foundation
import UIKit

extension UITableViewCell {
    static var reuseIdentifier: String {
        String(describing: self)
    }
}

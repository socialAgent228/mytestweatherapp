//
//  File.swift
//  TestWeatherApp
//
//  Created by  apple on 4/11/21.
//

import Foundation
import UIKit

extension UICollectionViewCell {
    static var reuseIdentifier: String {
        String(describing: self)
    }
}

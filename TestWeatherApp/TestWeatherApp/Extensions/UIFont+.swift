//
//  UIFont+.swift
//  TestWeatherApp
//
//  Created by  apple on 4/15/21.
//

import Foundation
import UIKit

extension UIFont {
    
   static func heleveticaFont(size: CGFloat) -> UIFont {
        return UIFont(name: "Helvetica-Light", size: size)!
   }
   
}

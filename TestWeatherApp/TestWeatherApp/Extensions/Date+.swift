//
//  Date+.swift
//  TestWeatherApp
//
//  Created by  apple on 4/15/21.
//

import Foundation

extension Date {
    
    func dayOfWeek() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        return formatter.string(from: self).capitalized
    }
    
    func hourString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH"
        return formatter.string(from: self)
    }
}

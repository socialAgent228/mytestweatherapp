//
//  WeatherShowModel.swift
//  TestWeatherApp
//
//  Created by  apple on 4/15/21.
//

import Foundation

struct WeatherShowModel {
    
    let header: Header
    let daily: [Daily]
    let hourly: [Hourly]
    let additional: Additional
    
    
    init(cityName: String, model: WeatherModelContainer) {
        var headerWeatherString: WeatherType = .clear
        model.current.weather.forEach({
            if let type = WeatherType(rawValue: $0.main) {
                headerWeatherString = type
            }
        })
        self.header = Header(
            cityName: cityName,
            currentTemp: "\(Int(model.current.temp))°",
            weatherType: headerWeatherString,
            feelsLike: "Feels like: \(Int(model.current.feels_like))°"
        )
        self.daily = model.daily.map({ Daily(model: $0) })
        self.hourly = model.hourly.map({ Hourly(model: $0) })
        self.additional = Additional(model: model)
    }
}

// MARK: WeatherHeaderShowModel

extension WeatherShowModel {
    
    struct Header {
        let cityName: String
        let currentTemp: String
        let weatherType: WeatherType
        let feelsLike: String
    }
    
}

// MARK: WeatherAdditionalInfoModel

extension WeatherShowModel {
    
    struct Additional {
        let sunrise: String
        let sunset: String
        let windSpeed: String
        let visibility: String
        let humidity: String
        let pressure: String
        let clouds: String
        
        init(model: WeatherModelContainer) {
            let formatter1 = DateFormatter()
            formatter1.timeStyle = .short
            self.sunset = formatter1.string(from: Date(timeIntervalSince1970: model.current.sunset))
            self.sunrise = formatter1.string(from: Date(timeIntervalSince1970: model.current.sunrise))
            self.windSpeed = "\(Int(model.current.wind_speed)) m/s"
            self.visibility = "\(model.current.visibility / 1000.0) km"
            self.humidity = "\(Int(model.current.humidity))%"
            self.pressure = "\(Int(model.current.pressure)) hPa"
            self.clouds = "\(Int(model.current.clouds))%"
        }
    }
    
}

// MARK: WeatherDailyShowModel

extension WeatherShowModel {
    
    struct Daily {
        let dateString: String
        let temperatureDay: String
        let temperatureNight: String
        let min: String
        let max: String
        let weatherType: WeatherType
        let popPercentsString: String
        
        init(model: WeatherModelContainer.WeatherDailyModel) {
            self.dateString = Date(timeIntervalSince1970: model.dt).dayOfWeek()
            self.temperatureDay = "\(Int(model.temp.day))"
            self.temperatureNight = "\(Int(model.temp.night))"
            self.min = "\(Int(model.temp.min))"
            self.max = "\(Int(model.temp.max))"
            var headerWeatherString: WeatherType = .clear
            model.weather.forEach({
                if let type = WeatherType(rawValue: $0.main) {
                    headerWeatherString = type
                }
            })
            self.weatherType = headerWeatherString
            self.popPercentsString = model.pop == 0 ? " " : "\(Int(model.pop * 100))%"
        }
    }
    
}

// MARK: WeatherHourlyShowModel

extension WeatherShowModel {
    
    struct Hourly {
        let dateString: String
        let temp: String
        let weatherType: WeatherType
        let popPercentsString: String
        
        init(model: WeatherModelContainer.WeatherHourlyModel) {
            self.dateString = Date(timeIntervalSince1970: model.dt).hourString()
            self.temp = "\(Int(model.temp))°"
            var headerWeatherString: WeatherType = .clear
            model.weather.forEach({
                if let type = WeatherType(rawValue: $0.main) {
                    headerWeatherString = type
                }
            })
            self.weatherType = headerWeatherString
            self.popPercentsString = model.pop == 0 ? " " : "\(Int(model.pop * 100))%"
        }
    }
}

// MARK: WeatherType

extension WeatherShowModel {
    
    enum WeatherType: String {
        case snow = "Snow"
        case rain = "Rain"
        case clear = "Clear"
        case clouds = "Cloudy"
        
        var imageName: String {
            switch self {
            case .clear:
                return "clearImage"
            case .clouds:
                return "cloudsImage"
            case .rain:
                return "rainImage"
            case .snow:
                return "snowImage"
            }
        }
    }
}

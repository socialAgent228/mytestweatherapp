//
//  WeatherModels.swift
//  TestWeatherApp
//
//  Created by  apple on 4/7/21.
//

import Foundation

// MARK: - WeatherResponse

struct WeatherModelContainer: Codable, Hashable {
    var current: WeatherCurrentModel
    var daily: [WeatherDailyModel]
    var hourly: [WeatherHourlyModel]
}

// MARK: - WeatherCurrentModel

extension WeatherModelContainer {
    
    struct WeatherCurrentModel: Codable, Hashable {
        let sunrise: Double
        let sunset: Double
        let temp: Double
        let humidity: Double
        let pressure: Double
        let clouds: Double
        let feels_like: Double
        let wind_speed: Double
        let visibility: Double
        let weather: [WeatherTypeModel]
    }
    
}

// MARK: - WeatherDailyModel

extension WeatherModelContainer {
    
    struct WeatherDailyModel: Codable, Hashable {
        
        struct WeatherDailyTempModel: Codable, Hashable {
            let day: Double
            let night: Double
            let min: Double
            let max: Double
        }
        
        let dt: Double
        let temp: WeatherDailyTempModel
        let pop: Double
        let weather: [WeatherTypeModel]
    }
    
}

// MARK: - WeatherHourlyModel

extension WeatherModelContainer {
    
    struct WeatherHourlyModel: Codable, Hashable {
        let dt: Double
        let temp: Double
        let pop: Double
        let weather: [WeatherTypeModel]
    }
    
}

// MARK: - WeatherTypeModel

extension WeatherModelContainer {
    
    struct WeatherTypeModel: Codable, Hashable {
        let main: String
        let icon: String
    }
    
}

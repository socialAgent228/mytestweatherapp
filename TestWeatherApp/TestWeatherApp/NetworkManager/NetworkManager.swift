//
//  NetworkManager.swift
//  TestWeatherApp
//
//  Created by  apple on 4/7/21.
//

import Foundation
import UIKit

enum NetworkErrors: String, Error  {
    case dataError = "Error: Data did not come."
    case decodeError = "Error: Can't decode models from data."
    case urlError = "Error: invalid url"
    case unknownError = "Error: unknown error"
}

// MARK: Manager itself
class NetworkManager {
    
    private static func getUrlString(lat: String, lon: String) -> String {
        let urlBase = "https://api.openweathermap.org/data/2.5/onecall?"
        let apiKey = "4c6ff53a1fbba4e52bf5673773855726"
        let exclude = "minutely,alerts"
        let units = "metric"
        return urlBase + "lat=" + lat + "&lon=" + lon + "&exclude=" + exclude + "&units=" + units + "&appid=" + apiKey
    }
    
    static func getWeather(lat: String, lon: String, completionHandler: @escaping (String?, WeatherModelContainer?) -> Void) {
        
        guard let url = URL(string: getUrlString(lat: lat, lon: lon)) else {
            completionHandler("url error", nil)
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
                do {
                    let decodedResponse = try JSONDecoder().decode(WeatherModelContainer.self, from: data)
                    completionHandler(nil, decodedResponse)
                    print(decodedResponse)
                    return
                } catch {
                    completionHandler(error.localizedDescription, nil)
                    return
                }
            } else {
                completionHandler("unknown error", nil)
                return
            }
            
        }.resume()
    }
}

//
//  DataManager.swift
//  TestWeatherApp
//
//  Created by apple on 4/11/21.
//

import Foundation

class DataManager {
    
    private struct Keys {
        static let kWeatherModel = "kWeatherModel"
        static let kCityName = "kCityName"
    }
    
    private static func setCityName(_ name: String) {
        UserDefaults.standard.set(name, forKey: Keys.kCityName)
    }
    
    private static func getCityName() -> String {
        return UserDefaults.standard.string(forKey: Keys.kCityName) ?? ""
    }
    
    private static func getWeather() -> WeatherModelContainer? {
        if let data = UserDefaults.standard.data(forKey: Keys.kWeatherModel) {
            return try? JSONDecoder().decode(WeatherModelContainer.self, from: data)
        }
        return nil
    }
    
    private static func setWeather(_ model: WeatherModelContainer) {
        let data = try? JSONEncoder().encode(model)
        UserDefaults.standard.set(data, forKey: Keys.kWeatherModel)
    }
    
    static func setModel(cityName: String, model: WeatherModelContainer) {
        setCityName(cityName)
        setWeather(model)
    }
    
    static func getModel() -> WeatherShowModel? {
        if let model = getWeather() {
            return WeatherShowModel(cityName: getCityName(), model: model)
        }
        return nil
    }
}


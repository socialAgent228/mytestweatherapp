//
//  ViewController.swift
//  TestWeatherApp
//
//  Created by  apple on 4/7/21.
//

import UIKit
import CoreLocation


// MARK: MainVC
class MainViewController: UIViewController {

    // MARK: - Properties
    //models
    private var weatherModel: WeatherShowModel? = DataManager.getModel()
    
    //UI Properties
    private var tableView = UITableView()
    private var backgroundView = UIView()
    
    // location-related properties
    private var locationManager: CLLocationManager?
    private var currentLocation: CLLocation?
    private var city: String?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getLocation()
        self.setupBackgroundView()
        self.setupTableView()
        
    }
    
    // MARK: -Methods
    
    private func getWeather(lat: String, lon: String) {
        NetworkManager.getWeather(lat: lat, lon: lon) {
            (error, response) in
            if let error = error {
                print(error)
            }
            if let models = response {
                DataManager.setModel(cityName: self.city!, model: models)
                self.weatherModel = DataManager.getModel()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
}

// MARK: - LocationManagerDelegate
extension MainViewController: CLLocationManagerDelegate {
    
    private func getLocation() {
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager = CLLocationManager()
            self.locationManager?.delegate = self
            self.locationManager?.desiredAccuracy = kCLLocationAccuracyKilometer
            self.locationManager?.requestWhenInUseAuthorization()
            self.locationManager?.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.currentLocation = location
            
            let lat: String = String(self.currentLocation?.coordinate.latitude ?? 0)
            let lon: String = String(self.currentLocation?.coordinate.longitude ?? 0)
            
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                }
                if let placemarks = placemarks {
                    if placemarks.count > 0 {
                        let placemark = placemarks[0]
                        if let city = placemark.locality {
                            self.city = city
                        }
                    }
                }
            }
            getWeather(lat: lat, lon: lon)
        }
    }
}


// MARK: - TableViewDelegate
extension MainViewController: UITableViewDelegate {
    
    fileprivate func setupBackgroundView() {
        
        let imageView = UIImageView(image: UIImage(named: "blueSky"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        let blurView = UIView()
        blurView.translatesAutoresizingMaskIntoConstraints = false
        blurView.backgroundColor = UIColor.systemGray.withAlphaComponent(0.25)
        
        self.view.addSubview(backgroundView)
        self.backgroundView.addSubview(imageView)
        self.backgroundView.addSubview(blurView)
        self.backgroundView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            self.backgroundView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0.0),
            self.backgroundView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0.0),
            self.backgroundView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0.0),
            self.backgroundView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0.0),
            
            imageView.topAnchor.constraint(equalTo: self.backgroundView.topAnchor, constant: 0.0),
            imageView.leadingAnchor.constraint(equalTo: self.backgroundView.leadingAnchor, constant: 0.0),
            imageView.trailingAnchor.constraint(equalTo: self.backgroundView.trailingAnchor, constant: 0.0),
            imageView.bottomAnchor.constraint(equalTo: self.backgroundView.bottomAnchor, constant: 0.0),
            
            blurView.topAnchor.constraint(equalTo: self.backgroundView.topAnchor, constant: 0.0),
            blurView.leadingAnchor.constraint(equalTo: self.backgroundView.leadingAnchor, constant: 0.0),
            blurView.trailingAnchor.constraint(equalTo: self.backgroundView.trailingAnchor, constant: 0.0),
            blurView.bottomAnchor.constraint(equalTo: self.backgroundView.bottomAnchor, constant: 0.0)
        ])
    }
    
    func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = .clear
        
        self.view.addSubview(self.tableView)
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0.0),
            self.tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0.0),
            self.tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0.0),
            self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0.0)
        ])

        self.tableView.register(HeaderTableViewCell.self, forCellReuseIdentifier: HeaderTableViewCell.reuseIdentifier)
        self.tableView.register(ScrollTableViewCell.self, forCellReuseIdentifier: ScrollTableViewCell.reuseIdentifier)
        self.tableView.register(DailyTableViewCell.self, forCellReuseIdentifier: DailyTableViewCell.reuseIdentifier)
        self.tableView.register(AdditionalTableViewCell.self, forCellReuseIdentifier: AdditionalTableViewCell.reuseIdentifier)
    }
    
    
    fileprivate enum Sections: Int, CaseIterable {
        
        fileprivate enum AdditionalRows: Int, CaseIterable {
            case sunrise
            case sunset
            case humidity
            case cloudness
            case windSpeed
            
            var titleString: String {
                switch self {
                case .sunrise:
                    return "sunrise"
                case .sunset:
                    return "sunset"
                case .humidity:
                    return "humidity"
                case .cloudness:
                    return "cloudness"
                case .windSpeed:
                    return "windSpeed"
                }
            }
            
            func valueForType(model: WeatherShowModel.Additional) -> String {
                switch self {
                case .sunrise:
                    return model.sunrise
                case .sunset:
                    return model.sunset
                case .humidity:
                    return model.humidity
                case .cloudness:
                    return model.clouds
                case .windSpeed:
                    return model.windSpeed
                }
            }
        }
        
        case header
        case hourly
        case daily
        case additional
        
        var rowsCount: Int {
            switch self {
            case .header:
                return 1
            case .hourly:
                return 1
            case .daily:
                return 8
            case .additional:
                return AdditionalRows.allCases.count
            }
        }
        
        var rowHeight: CGFloat {
            switch self {
            case .header:
                return HeaderTableViewCell.rowHeight
            case .hourly:
                return ScrollTableViewCell.rowHeight
            case .daily:
                return DailyTableViewCell.rowHeight
            case .additional:
                return AdditionalTableViewCell.rowHeight
            }
        }
    }
    
}


// MARK: - TableViewDataSource
extension MainViewController: UITableViewDataSource {
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = self.weatherModel else {
            return UITableViewCell()
        }
        switch Sections(rawValue: indexPath.section) ?? .header {
        case .header:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: HeaderTableViewCell.reuseIdentifier, for: indexPath) as! HeaderTableViewCell
            cell.setup(model: model.header)
            cell.selectionStyle = .none
            return cell
        case .hourly:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: ScrollTableViewCell.reuseIdentifier, for: indexPath) as! ScrollTableViewCell
            cell.setup(models: model.hourly)
            cell.selectionStyle = .none
            return cell
        case .daily:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: DailyTableViewCell.reuseIdentifier, for: indexPath) as! DailyTableViewCell
            cell.setup(model: model.daily[indexPath.row])
            cell.selectionStyle = .none
            return cell
        case .additional:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: AdditionalTableViewCell.reuseIdentifier, for: indexPath) as! AdditionalTableViewCell
            let type = Sections.AdditionalRows(rawValue: indexPath.row)!
            cell.setup(title: type.titleString, value: type.valueForType(model: model.additional))
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        Sections.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Sections(rawValue: section)!.rowsCount
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Sections(rawValue: indexPath.section)!.rowHeight
    }
    
    
}
//поменяй имя контроллера на WeatherViewController




//
//  DailyTableViewCell.swift
//  TestWeatherApp
//
//  Created by  apple on 4/11/21.
//

import UIKit

class DailyTableViewCell: UITableViewCell {

    static let rowHeight: CGFloat = 55
    
    private let labelsStackView = UIStackView()
    
    private let dateLabel = UILabel()
    private let weatherImageView = UIImageView()
    private let popLabel = UILabel()
    private let dayTemperatureLabel = UILabel()
    private let nightTemperatureLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = .clear
        
        self.labelsStackView.axis = .horizontal
        self.labelsStackView.spacing = 12
        self.labelsStackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(labelsStackView)
        
        NSLayoutConstraint.activate([
            self.labelsStackView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
            self.labelsStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            self.labelsStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
        ])
        
        weatherImageView.contentMode = .scaleAspectFit
        
        [dateLabel, weatherImageView, popLabel, dayTemperatureLabel, nightTemperatureLabel].forEach({
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.labelsStackView.addArrangedSubview($0)
        })
        
        [dateLabel, dayTemperatureLabel].forEach({
            $0.font = UIFont.heleveticaFont(size: 18)
            $0.textColor = .white
        })
        
        [popLabel, dayTemperatureLabel, nightTemperatureLabel].forEach({
            $0.textAlignment = .right
        })
        
        nightTemperatureLabel.textColor = .systemGray3
        
        popLabel.textColor = .systemBlue
        popLabel.font = UIFont.heleveticaFont(size: 18)
        
        NSLayoutConstraint.activate([
            self.weatherImageView.heightAnchor.constraint(equalToConstant: 20),
            self.weatherImageView.widthAnchor.constraint(equalToConstant: 20),
            
            self.popLabel.widthAnchor.constraint(equalToConstant: 40),
            self.dayTemperatureLabel.widthAnchor.constraint(equalToConstant: 40),
            self.nightTemperatureLabel.widthAnchor.constraint(equalToConstant: 40),
            
        ])
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(model: WeatherShowModel.Daily) {
        self.dateLabel.text = model.dateString
        self.weatherImageView.image = UIImage(named: model.weatherType.imageName)
        self.popLabel.text = model.popPercentsString
        self.dayTemperatureLabel.text = model.temperatureDay
        self.nightTemperatureLabel.text = model.temperatureNight
    }

}

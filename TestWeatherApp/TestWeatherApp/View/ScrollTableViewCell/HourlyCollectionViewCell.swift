//
//  HourlyCollectionViewCell.swift
//  TestWeatherApp
//
//  Created by  apple on 4/15/21.
//

import UIKit

class HourlyCollectionViewCell: UICollectionViewCell {
    
    static let rowSize: CGSize = CGSize(width: 50, height: 110)
    
    private let labelsStackView = UIStackView()
    
    private let dateLabel = UILabel()
    private let popLabel = UILabel()
    private let weatherImageView = UIImageView()
    private let temperatureLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        
        self.labelsStackView.axis = .vertical
        self.labelsStackView.distribution = .fillEqually
        self.labelsStackView.spacing = 3
        
        self.weatherImageView.contentMode = .scaleAspectFit
        
        [dateLabel, popLabel, temperatureLabel].forEach({
            $0.textAlignment = .center
            $0.textColor = .white
            $0.translatesAutoresizingMaskIntoConstraints = false
        })
        
        [dateLabel, popLabel, weatherImageView, temperatureLabel].forEach({
            self.labelsStackView.addArrangedSubview($0)
        })
        
        dateLabel.font = UIFont.heleveticaFont(size: 17)
        popLabel.font = UIFont.heleveticaFont(size: 14)
        temperatureLabel.font = UIFont.heleveticaFont(size: 18)
        
        self.labelsStackView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(labelsStackView)
        
        NSLayoutConstraint.activate([
            self.weatherImageView.widthAnchor.constraint(equalToConstant: 17),
            self.weatherImageView.heightAnchor.constraint(equalToConstant: 17),
            self.weatherImageView.bottomAnchor.constraint(equalTo: self.temperatureLabel.topAnchor, constant: -15)
        ])
        
        NSLayoutConstraint.activate([
            self.labelsStackView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
            self.labelsStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 2),
            self.labelsStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -1)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(model: WeatherShowModel.Hourly) {
        self.dateLabel.text = model.dateString
        self.popLabel.text = model.popPercentsString
        self.weatherImageView.image = UIImage(named: model.weatherType.imageName)
        self.temperatureLabel.text = model.temp
    }
    
}

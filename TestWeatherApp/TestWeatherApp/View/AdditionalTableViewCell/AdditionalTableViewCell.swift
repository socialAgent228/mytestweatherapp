//
//  AdditionalTableViewCell.swift
//  TestWeatherApp
//
//  Created by apple on 4/11/21.
//

import UIKit

class AdditionalTableViewCell: UITableViewCell {
    
    static let rowHeight: CGFloat = 70
    
    private let labelsStackView = UIStackView()
    
    private let titleLabel = UILabel()
    private let valueLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = .clear
        
        self.labelsStackView.axis = .vertical
        self.labelsStackView.spacing = 5
        
        self.labelsStackView.translatesAutoresizingMaskIntoConstraints = false
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.valueLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.font = UIFont.heleveticaFont(size: 12)
        valueLabel.font = UIFont.heleveticaFont(size: 24)
        titleLabel.textColor = .darkText
        valueLabel.textColor = .white
        
        self.addSubview(labelsStackView)
        self.labelsStackView.addArrangedSubview(titleLabel)
        self.labelsStackView.addArrangedSubview(valueLabel)
        
        NSLayoutConstraint.activate([
            self.labelsStackView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
            self.labelsStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            self.labelsStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
        ])
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(title: String, value: String) {
        self.titleLabel.text = title.uppercased()
        self.valueLabel.text = value
    }
    
}

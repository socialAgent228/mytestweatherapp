//
//  HeaderTableViewCell.swift
//  TestWeatherApp
//
//  Created by  apple on 4/11/21.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {
    
    static let rowHeight: CGFloat = 250
    
    private let labelsStackView = UIStackView()
    
    private let cityNameLabel = UILabel()
    private let weatherLabel = UILabel()
    private let temperatureLabel = UILabel()
    private let feelsLikeLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = .clear
        
        self.labelsStackView.axis = .vertical
        self.labelsStackView.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(labelsStackView)
        
        [cityNameLabel, weatherLabel, temperatureLabel, feelsLikeLabel].forEach({
            $0.textColor = .white
            $0.textAlignment = .center
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.labelsStackView.addArrangedSubview($0)
        })
           
        cityNameLabel.font = UIFont.heleveticaFont(size: 35)
        weatherLabel.font = UIFont.heleveticaFont(size: 17)
        temperatureLabel.font = UIFont.heleveticaFont(size: 65)
        feelsLikeLabel.font = UIFont.heleveticaFont(size: 17)
        
        NSLayoutConstraint.activate([
            self.labelsStackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 50),
            self.labelsStackView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0)
        ])
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(model: WeatherShowModel.Header) {
        self.cityNameLabel.text = model.cityName
        self.temperatureLabel.text = model.currentTemp
        self.weatherLabel.text = model.weatherType.rawValue.capitalized
        self.feelsLikeLabel.text = model.feelsLike
    }

}
